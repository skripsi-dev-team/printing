-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 21, 2019 at 06:46 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi_printing`
--

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nama_pelanggan` varchar(60) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `alamat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `username`, `password`, `email`, `nama_pelanggan`, `no_telp`, `alamat`) VALUES
(1, 'gerrard', 'f4542db9ba30f7958ae42c113dd87ad21fb2eddb', 'gerrard@lfc.com', 'Steven Gerrard', '082820', 'Liverpool '),
(2, 'naruto', 'f4542db9ba30f7958ae42c113dd87ad21fb2eddb', 'naruto@email.com', 'Uzumaki Naruto', '0820802', 'Konohagakure'),
(3, 'refojunior', 'f4542db9ba30f7958ae42c113dd87ad21fb2eddb', 'refojunior@gmail.com', 'Refo Junior', '0820802', 'Denpasar'),
(4, 'fernandotorres', 'f4542db9ba30f7958ae42c113dd87ad21fb2eddb', 'torres@lfc.com', 'Fernando Torres', '088202', 'Spain'),
(5, 'ahmad123', 'f4542db9ba30f7958ae42c113dd87ad21fb2eddb', 'ahmad@email.com', 'Ahmad Zulkarnain', '082802', 'Denpasar');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `id_pemesanan` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `ongkir` enum('0','1') NOT NULL,
  `bukti_transfer` varchar(255) NOT NULL,
  `nominal_pembayaran` int(11) NOT NULL,
  `status_pembayaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id_pembayaran`, `id_pemesanan`, `tanggal`, `ongkir`, `bukti_transfer`, `nominal_pembayaran`, `status_pembayaran`) VALUES
(3, 4, '2019-10-21 09:47:58', '0', '5dad62ae4cf2f.jpg', 90000, 1),
(4, 1, '2019-10-21 17:30:57', '1', '5dadcf31cba0a.jpg', 70000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan`
--

CREATE TABLE `pemesanan` (
  `id_pemesanan` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `tipe_print` enum('kartu nama','brosur','stiker') NOT NULL,
  `tanggal` datetime NOT NULL,
  `file_pesanan` varchar(255) NOT NULL,
  `jenis_kertas` varchar(50) NOT NULL,
  `format` varchar(50) NOT NULL,
  `sisi_cetak` enum('1','2') NOT NULL,
  `quantity` int(11) NOT NULL,
  `finishing` varchar(50) NOT NULL,
  `total_biaya` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemesanan`
--

INSERT INTO `pemesanan` (`id_pemesanan`, `id_pelanggan`, `tipe_print`, `tanggal`, `file_pesanan`, `jenis_kertas`, `format`, `sisi_cetak`, `quantity`, `finishing`, `total_biaya`, `status`) VALUES
(1, 1, 'kartu nama', '0000-00-00 00:00:00', '5dac57ae34751.jpg', 'art carton', '9 x 5,5 cm', '1', 2, 'dof', 60000, 3),
(2, 1, 'brosur', '0000-00-00 00:00:00', '5dac5c9964bba.jpg', 'matt paper', 'A5', '2', 60, '', 288000, 1),
(3, 1, 'stiker', '0000-00-00 00:00:00', '5dac5d0d32d34.png', 'Stiker Doff', '7 x 7 cm', '1', 50, '', 75000, 1),
(4, 1, 'kartu nama', '2019-10-21 09:41:32', '5dad612c9838b.jpg', 'matt paper', '9 x 5,5 cm', '1', 3, 'dof', 90000, 5);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `no_telp` varchar(16) NOT NULL,
  `jabatan` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `nama_petugas`, `no_telp`, `jabatan`) VALUES
(7, 'sadiomane', 'f4542db9ba30f7958ae42c113dd87ad21fb2eddb', 'Sadio Mane', '0820820', 2),
(8, 'mosalah', 'f4542db9ba30f7958ae42c113dd87ad21fb2eddb', 'Mohamed Salah', '0820802', 3),
(9, 'alisson', '601f1889667efaebb33b8c12572835da3f027f78', 'Alisson Becker', '2092092', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
