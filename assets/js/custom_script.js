$(document).ready(function () {
	$('.custDate').change(function () {
		var firstDate = $('.firstDate').val();
		var secondDate = '';

		if (!$('.secondDate').val()) {
			$('.secondDate').attr('min', firstDate);
		} else {
			secondDate = $('.secondDate').val();
			if (firstDate > secondDate) {
				$('.secondDate').attr('min', firstDate);
				$('.secondDate').val(firstDate);
			} else {
				$('.secondDate').attr('min', firstDate);
			}
		}
	});

	$('.custDate').submit(function () {
		var firstDate = $('.firstDate').val();
		var secondDate = $('.secondDate').val();

		if (!firstDate) {
			$('.tgl_a').append('Tanggal Kosong!');
			return false;
		} else {
			$('.tgl_a').empty();
		}

		if (!secondDate) {
			$('.tgl_b').append('Tanggal Kosong!');
			return false;
		}
	});
});
