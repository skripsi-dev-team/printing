<?php 


function urlHasPrefix($prefix){
    $url = current_url();
    if (strpos($url, $prefix) > 0) {
        return true;
    }

    return false;
}

function getJabatan($val){
    $level = [
        1 => 'kasir',
        2 => 'petugas printing',
        3 => 'administrator'
    ];
    
    return $level[$val];
}

function isKasirAdmin($jabatan){
    if($jabatan == 1 || $jabatan == 3){
        return true;
    }else{
        return false;
    }
}

function isPetugas($jabatan){
    if($jabatan == 2){
        return true;
    }else{
        return false;
    }
}

function isAdmin($jabatan){
    if($jabatan == 3){
        return true;
    }else{
        return false;
    }
}

function cekLogin(){
    $CI =& get_instance();
    if(!$CI->session->userdata('login_backend')){
        $CI->session->set_flashdata('error', 'Anda belum login!');
        redirect(base_url('admin/'));
    }
}

function checkPelangganSession(){
    $CI =& get_instance();
    if(isset($CI->session->id_pelanggan)){
        redirect('home');
    }
}

function checkPelanggan(){
    $CI =& get_instance();
    if(!isset($CI->session->id_pelanggan)){
        redirect('home');
    }
}

function removeMin($nama){
    $nama = str_replace('-', ' ', $nama);
    return $nama;
}

function statusPemesanan(){
    $status = [
        '1' => 'Menunggu Pembayaran',
        '2' => 'Menunggu Konfirmasi Pembayaran',
        '3' => 'Pembayaran berhasil',
        '4' => 'Sedang dikerjakan',
        '5' => 'Selesai',
        '6' => 'Pesanan dibatalkan'
    ];
    return $status;
}

function setStatus($status){
    $set = '';
    if($status == 1){
        $set = '<span class="badge badge-warning">Menunggu Pembayaran</span>';
    }

    if($status == 2){
        $set = '<span class="badge badge-info">Menunggu Konfirmasi Pembayaran</span>';
    }

    if($status == 3){
        $set = '<span class="badge badge-success">Pembayaran berhasil</span>';
    }

    if($status == 4){
        $set = '<span class="badge badge-secondary">Sedang dikerjakan</span>';
    }

    if($status == 5){
        $set = '<span class="badge badge-success">Selesai</span>';
    }

    if($status == 6){
        $set = '<span class="badge badge-danger">Pesanan dibatalkan</span>';
    }

    return $set;
}


function getKabupaten(){
    return [
        'Denpasar',
        'Badung',
        'Bangli',
        'Buleleng',
        'Gianyar',
        'Jembrana',
        'Karangasem',
        'Klungkung',
        'Tabanan'
    ];
}







?>