<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('backend/part/head.php') ?>
</head>

<body>
    <!-- navbar -->
    <?php $this->load->view('backend/part/navbar.php') ?>

    <div class="wrapper">     
        <!-- Sidebar Holder -->
        <?php $this->load->view('backend/part/sidebar.php') ?>

        <!-- Page Content Holder -->
        <div id="content">
            
            <!-- flash message -->
            <?php $this->load->view('backend/part/flash.php') ?>

            <h1 style="text-transform:capitalize"><?= $title ?></h1>
            <hr>
            <!-- content -->
            <?php $this->load->view('backend/'.$content); ?>
            <!-- content -->
        </div>
    </div>


    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <?php $this->load->view('backend/part/javascript.php') ?>

    <?php if(isset($month)) : ?>

    <script src="<?= base_url('assets/chart.js/Chart.min.js') ?>"></script>
    <script>

        var BARCHARTEXMPLE    = $('#barChartExample');

        var barChartExample = new Chart(BARCHARTEXMPLE, {
        type: 'bar',
        data: {
            labels: <?= $month ?>,
            datasets: [
                {                    
                    label: "Total Penjualan Perbulan",
                    backgroundColor: [
                        'rgba(203, 203, 203, 0.6)',
                        'rgba(203, 203, 203, 0.6)',
                        'rgba(203, 203, 203, 0.6)',
                        'rgba(203, 203, 203, 0.6)',
                        'rgba(203, 203, 203, 0.6)',
                        'rgba(203, 203, 203, 0.6)',
                        'rgba(203, 203, 203, 0.6)',
                        'rgba(203, 203, 203, 0.6)',
                        'rgba(203, 203, 203, 0.6)',
                        'rgba(203, 203, 203, 0.6)',
                        'rgba(203, 203, 203, 0.6)',
                        'rgba(203, 203, 203, 0.6)'
                    ],
                    borderColor: [
                        'rgba(203, 203, 203, 1)',
                        'rgba(203, 203, 203, 1)',
                        'rgba(203, 203, 203, 1)',
                        'rgba(203, 203, 203, 1)',
                        'rgba(203, 203, 203, 1)',
                        'rgba(203, 203, 203, 1)',
                        'rgba(203, 203, 203, 1)',
                        'rgba(203, 203, 203, 1)',
                        'rgba(203, 203, 203, 1)',
                        'rgba(203, 203, 203, 1)',
                        'rgba(203, 203, 203, 1)',
                        'rgba(203, 203, 203, 1)'
                    ],
                    borderWidth: 1,
                    data: <?= $perBulan ?>,
                }
            ]
        }
    });

    </script>
    <?php endif ?>

    <!-- JS NOTIFICATION -->
    <script>        
        $(document).ready(function(){
            var is_click = 1;
            $('.btn-notification').click(function(){
                is_click = 2;
                $('.content-notification').slideToggle();              
                
                    $(this).click(function(e){
                        if(is_click == 2) {
                           //do function ajax
                           e.preventDefault();
                           $.ajax({
                            'type': 'get',
                            'url': "<?= base_url() ?>admin/dashboard/updatenotif/<?= $this->session->jabatan ?>",
                            'data': {
                                'untuk': "<?= $this->session->jabatan ?>" 
                            },
                            success:function(result){
                                //do something but nothing (i have no idea)
                            },
                            error:function(result){
                                alert('failed');
                            }
                           });
                            
                        }
                        
                        is_click = 1; 
                    }); 
                              
            });

        });

    </script>
</body>

</html>