<div class="row">
    <div class="col-md-12">
        <a href="<?= base_url('admin/petugas/create') ?>" class="btn btn-primary mb-3">Tambah Petugas</a>
    </div>
    
    <div class="col-md-12">
        <table class="table" id="myTable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Petugas</th>
                    <th>Username</th>
                    <th>No. Telp</th>
                    <th>Jabatan</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; foreach($petugas as $data) : ?>
                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $data->nama_petugas ?></td>
                    <td><?= $data->username ?></td>
                    <td><?= $data->no_telp ?></td>
                    <td><?= getJabatan($data->jabatan) ?></td>
                    <td>
                        <form action="<?= base_url('admin/petugas/delete/' .$data->id_petugas) ?>" method="post">
                            <a href="<?= base_url('admin/petugas/edit/'.$data->id_petugas) ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a> 
                            <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin akan hapus data?')"><i class="fa fa-trash"></i> Hapus</button>
                        </form>
                
                    </td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>