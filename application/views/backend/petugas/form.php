<div class="row">
    <div class="col-md-6">
        <?php if(!isset($input->id_petugas)) { ?>
        <form action="<?= base_url('admin/petugas/store') ?>" method="post">
        <?php } else { ?>
        <form action="<?= base_url('admin/petugas/update/'.$input->id_petugas) ?>" method="post">
        <?php } ?>
            <div class="form-group">
                <label>Nama Petugas</label>
                <input type="text" name="nama_petugas" class="form-control" placeholder="Masukan nama petugas" value="<?= $input->nama_petugas ?>">
                <small class="error"><?php echo form_error('nama_petugas') ?></small>
            </div>
            <div class="form-group">
                <label>No Telp</label>
                <input type="number" name="no_telp" class="form-control" placeholder="Masukan no telp petugas" value="<?= $input->no_telp ?>">
                <small class="error"><?php echo form_error('no_telp') ?></small>
            </div>
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" class="form-control" placeholder="Masukan username petugas" value="<?= $input->username ?>">
                <small class="error"><?php echo form_error('username') ?></small>
            </div>
            <?php if(!isset($input->id_petugas))  { ?>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control" placeholder="Masukan password petugas">
                <small class="error"><?php echo form_error('password') ?></small>
            </div>
            <?php } ?>
            <div class="form-group">
                <label>Jabatan</label>
                <select name="jabatan" class="form-control">
                    <option value=""> - Pilih - </option>
                    <option value="1" <?= ($input->jabatan == 1) ? "selected" : ""; ?>>Kasir</option>
                    <option value="2" <?= ($input->jabatan == 2) ? "selected" : ""; ?>>Petugas Printing</option>
                    <option value="3" <?= ($input->jabatan == 3) ? "selected" : ""; ?>>Administrator</option>
                </select>
                <small class="error"><?php echo form_error('jabatan') ?></small>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success float-right" value="Simpan">
            </div>
        </form>
    </div>
</div>