<div class="row">
    <div class="col-md-6">
        <?php if(!isset($input->id_pelanggan)) { ?>
        <form action="<?= base_url('admin/pelanggan/store') ?>" method="post">
        <?php } else { ?>
        <form action="<?= base_url('admin/pelanggan/update/'.$input->id_pelanggan) ?>" method="post">
        <?php } ?>
            <div class="form-group">
                <label>Nama Pelanggan</label>
                <input type="text" name="nama_pelanggan" class="form-control" placeholder="Masukan nama pelanggan" value="<?= $input->nama_pelanggan ?>">
                <small class="error"><?php echo form_error('nama_pelanggan') ?></small>
            </div>
            <div class="form-group">
                <label>No Telp</label>
                <input type="number" name="no_telp" class="form-control" placeholder="Masukan no telp pelanggan" value="<?= $input->no_telp ?>">
                <small class="error"><?php echo form_error('no_telp') ?></small>
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <textarea name="alamat" class="form-control" placeholder="Masukan alamat pelanggan"><?= $input->alamat ?></textarea>
                <small class="error"><?php echo form_error('alamat') ?></small>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" class="form-control" placeholder="Masukan email pelanggan" value="<?= $input->email ?>">
                <small class="error"><?php echo form_error('email') ?></small>
            </div>
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" class="form-control" placeholder="Masukan username pelanggan" value="<?= $input->username ?>">
                <small class="error"><?php echo form_error('username') ?></small>
            </div>
            <?php if(!isset($input->id_pelanggan))  { ?>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control" placeholder="Masukan password pelanggan">
                <small class="error"><?php echo form_error('password') ?></small>
            </div>
            <?php } ?>
            
            <div class="form-group">
                <input type="submit" class="btn btn-success float-right" value="Simpan">
            </div>
        </form>
    </div>
</div>