<div class="row">
    <div class="col-md-12">
        <a href="<?= base_url('admin/pelanggan/create') ?>" class="btn btn-primary mb-3">Tambah Petugas</a>
    </div>
 
    <div class="col-md-12">
        <table class="table" id="myTable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Pelanggan</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>No Telp</th>
                    <th>Alamat</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; foreach($pelanggan as $data) { ?>
                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $data->nama_pelanggan ?></td>
                    <td><?= $data->username ?></td>
                    <td><?= $data->email ?></td>
                    <td><?= $data->no_telp ?></td>
                    <td><?= $data->alamat ?></td>
                    <td>
                        <form action="<?= base_url('admin/pelanggan/delete/' .$data->id_pelanggan) ?>" method="post">
                            <a href="<?= base_url('admin/pelanggan/edit/'.$data->id_pelanggan) ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a> 
                            <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin akan hapus data?')"><i class="fa fa-trash"></i></button>
                        </form>                
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>