<!DOCTYPE html>
<html>
<head>
    <title>Print Nota</title>

    <style type="text/css">
        .container{
            height: auto;
            width: 100%;
        }

        .row{
            height: auto;
            width: 100%;
        }

        .col{
            float: left;
            width: 32%;
        }

        .text-header{
            text-align: center;
            font-size: 2.5em;
            font-weight: bold;
        }

        .table{
            width: 100%;
        }

        .table thead{
            border-top: 1px solid #000;
            border-bottom: 1px solid #000;
        }

        .bottom{
            margin-top: 20%;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col">
                CV. <?= APP_NAME ?> TEGAK PEDIDI
            </div>
            <div class="col text-header">
               Nota
            </div>
            <div class="col" style="float: right">
                <table>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td><?= date('d-m-Y', strtotime($pemesanan->tanggal)) ?></td>
                    </tr>
                    <tr>
                        <td>No Pemesanan</td>
                        <td>:</td>
                        <td><?= $pemesanan->id_pemesanan ?></td>
                    </tr>
                </table>
            </div>
            <hr style="margin-top: 12%">
        </div>
        <div class="row">
            <table>
                <tr>
                    <td>Nama Pelanggan</td>
                    <td>:</td>
                    <td><?= $pemesanan->nama_pelanggan ?></td>
                </tr>
                <tr>
                    <td>No Telp</td>
                    <td>:</td>
                    <td><?= $pemesanan->no_telp ?></td>
                </tr>
                <tr>
                    <td>Nama Alamat</td>
                    <td>:</td>
                    <td><?= $pemesanan->alamat ?></td>
                </tr>
            </table>
        </div>
        <div class="row" style="margin-top: 5%">
            <?php $no = 1; ?>
            <table border="0" class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tipe Print</th>
                        <th>Jenis Kertas</th>
                        <th>Format</th>
                        <th>Sisi Cetak</th>
                        <th>Qty</th>
                        <th>Finishing</th>
                        <th>Biaya</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $pemesanan->tipe_print ?></td>
                        <td><?= $pemesanan->jenis_kertas ?></td>
                        <td><?= $pemesanan->format ?></td>
                        <td><?= $pemesanan->sisi_cetak ?></td>
                        <td><?= $pemesanan->quantity ?></td>
                        <td><?= $pemesanan->finishing ?></td>
                        <td>Rp. <?= number_format($pemesanan->total_biaya) ?></td>
                    </tr>
                    <?php if($pembayaran->ongkir == 1): ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td colspan="6">Ongkir</td>
                        <td>Rp. 10.000</td>
                    </tr>
                    <?php endif ?>
                </tbody>
            </table>
        </div>
        <div class="row bottom">
            <hr>
            <div class="col">
                <b>Status :</b> <br>Lunas <br>
                <b>Tanggal Pembayaran :</b> <br><?= date('d-m-Y', strtotime($pembayaran->tanggal)) ?><br>
            </div>
            <div class="col">
               <b>Keterangan :</b><br>
               <?= ($pembayaran->ongkir == 1) ? 'Diantar' : 'Tidak Diantar' ?>
            </div>
            <div class="col" style="text-align: right">
                <b>Total :</b> Rp. <?= number_format($pembayaran->nominal_pembayaran) ?>
            </div>
        </div>
    </div>
</body>
</html>