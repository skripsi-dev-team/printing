
<!-- content -->
<div class="row">
    <div class="col-md-4">
        <div class="card text-white bg-primary mb-3">
            <div class="card-header">
                <i class="fas fa-print"></i> Pesanan Masuk Hari Ini
            </div>
            <div class="card-body">
                <p class="card-text text-white">
                    <?= $pesanan_masuk->masuk ?> Pesanan
                </p>
            </div>
        </div>

        <div class="card text-white bg-primary mb-3">
            <div class="card-header">
                <i class="fas fa-print"></i> Total Pemesanan
            </div>
            <div class="card-body">
                <p class="card-text text-white">
                    <?= $pesanan_total->total ?> Pesanan
                </p>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <!-- chart -->
        <canvas id="barChartExample"></canvas>

        <p>Sistem pemesanan online CV. Garda Tegak Pedidi</p>
    </div>
</div>
<!-- content -->

