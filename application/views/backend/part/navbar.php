<nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <div class="container-fluid">
        <div class="navbar-header">
            <img src="<?= base_url('assets/img/brand.jpg') ?>" width="125" height="auto" class="bg-white" style="padding:0 19px">
        </div>
        <button type="button" id="sidebarCollapse" class="navbar-btn">
            <span></span>
            <span></span>
            <span></span>
        </button>

        
        
        <div class="navbar-text">
            <div class="btn-notification d-inline-block">
                <i class="fa fa-bell"></i><span class="badge badge-info label-pos"><?= $this->notification_model->unReadNotif($this->session->jabatan); ?></span>
                <div class="content-notification">
                    <?php foreach ($this->notification_model->getNotification($this->session->jabatan) as $row) : ?>                        
                    <ul>
                        <li>
                            <strong><?= $row->judul ?></strong> 
                            <?php if($row->dilihat == 0) : ?>
                                <span class="badge badge-info label-pos-inside">Baru</span> 
                            <?php endif ?>
                        </li>
                        <li><span class="badge badge-secondary"><?= $row->tanggal ?></span></li>
                        <li><?= $row->isi ?></li>
                    </ul>
                    <?php endforeach ?>
                </div>
            </div>
            <i class="fas fa-user"></i> Halo, <?= $this->session->userdata('nama_petugas') ?> <a href="<?= base_url('admin/login/logout') ?>"><button class="btn btn-danger"><i class="fas fa-power-off"></i> Logout</button></a>
        </div>
    </div>
</nav>