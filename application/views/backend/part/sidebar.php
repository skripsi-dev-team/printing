<nav id="sidebar">
    <ul class="list-unstyled components">
        <li>
            <a href="<?= base_url('admin/dashboard') ?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
        </li>
        <?php if($this->session->jabatan != 3) : ?>
    
        <li class="has-child">
            <a href="#pembayaran" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-box"></i> Data Pemesanan</a>
            <ul class="collapse list-unstyled <?= (urlHasPrefix('pemesanan')) ? "show" : ""; ?>" id="pembayaran">
                <li>
                    <a href="<?= base_url('admin/pemesanan-masuk') ?>" class="<?= (urlHasPrefix('pemesanan-masuk')) ? "active" : ""; ?>">Pemesanan Masuk</a>
                </li>
                <li>
                    <a href="<?= base_url('admin/pemesanan-proses') ?>" class="<?= (urlHasPrefix('pemesanan-proses')) ? "active" : ""; ?>">Pemesanan Diproses</a>
                </li>
                <li>
                    <a href="<?= base_url('admin/pemesanan-selesai') ?>" class="<?= (urlHasPrefix('pemesanan-selesai')) ? "active" : ""; ?>">Pemesanan Selesai</a>
                </li>
                <li>
                    <a href="<?= base_url('admin/pemesanan-batal') ?>" class="<?= (urlHasPrefix('pemesanan-batal')) ? "active" : ""; ?>">Pemesanan Batal</a>
                </li>
            </ul>
        </li>
        <?php endif ?>

        <?php if(isAdmin($this->session->userdata('jabatan'))): ?>
        <li>
            <a href="<?= base_url('admin/pelanggan') ?>" class="<?= (urlHasPrefix('pelanggan')) ? "active" : ""; ?>"><i class="fas fa-users"></i> Data Pelanggan</a>
        </li>
        <?php endif ?>

        <?php if(isAdmin($this->session->userdata('jabatan'))): ?>
        <li>
            <a href="<?= base_url('admin/petugas') ?>" class="<?= (urlHasPrefix('petugas')) ? "active" : ""; ?>"><i class="fas fa-user"></i> Data Petugas</a>
        </li>
        <?php endif ?>

        <?php if(isKasirAdmin($this->session->userdata('jabatan'))): ?>
       
        <?php endif ?>


        <?php if(isKasirAdmin($this->session->userdata('jabatan'))): ?>
        <li>
            <a href="<?= base_url('admin/laporan-penjualan') ?>" class="<?= (urlHasPrefix('laporan-penjualan')) ? "active" : ""; ?>"><i class="fas fa-file"></i> Laporan Penjualan</a>
        </li>
        <?php endif ?>
    </ul>
    <p class="footer text-center">
        Copyright &copy; 2019 CV. GARDA. All Rights Reserved.
    </p>
</nav>