<div class="row pemesanan-row">
    <div class="col-md-12">
        <table class="table table-pemesanan" id="myTable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Nama Pemesan</th>
                    <th>Tipe Pesanan</th>
                    <th>Jenis Kertas</th>
                    <th>Format</th>
                    <th>Finishing</th>
                    <th>File Pesanan</th>
                    <th>Sisi Cetak</th>
                    <th>Qty</th>
                    <th>Biaya</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($pesanan as $row): ?>
                    
                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $row->tanggal ?></td>
                    <td><?= $row->nama_pelanggan ?></td>
                    <td><?= $row->tipe_print ?></td>
                    <td><?= $row->jenis_kertas ?></td>
                    <td><?= $row->format ?></td>
                    <td><?= $row->finishing ?></td>
                    <td><img src="<?= base_url('assets/upload/'.$row->file_pesanan) ?>" alt="imgf-file" width="50px"></td>
                    <td><?= $row->sisi_cetak ?> Sisi</td>
                    <td><?= $row->quantity ?></td>
                    <td><?= $row->total_biaya ?></td>
                    <td><?= setStatus($row->status) ?></td>
                    <td><a href="<?= base_url('admin/pemesanan/detail/'.$row->id_pemesanan) ?>" class="btn btn-sm btn-outline-info">Periksa</a></td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>