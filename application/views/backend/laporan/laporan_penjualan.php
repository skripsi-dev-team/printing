<?php
    $tgl_a = '';
    $tgl_b = '';

    if(!empty($_GET)){
        $tgl_a = $_GET['tgl_a'];
        $tgl_b = $_GET['tgl_b'];
    }
?>

<div class="row pemesanan-row">
    <div class="col-md-12">
        <form action="<?= base_url('admin/laporan-penjualan') ?>" method="GET" class="custDate">
            <div class="row">
                <div class="col-md-6">
                    <label>Tanggal Awal</label>
                    <input type="date" name="tgl_a" value="<?= $tgl_a ?>" class="form-control firstDate">
                    <p class="text-danger tgl_a"></p>
                </div>
                <div class="col-md-6">
                    <label>Tanggal Akhir</label>
                    <input type="date" name="tgl_b" value="<?= $tgl_b ?>" class="form-control secondDate">
                    <p class="text-danger tgl_b"></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <input type="submit" value="Tampilkan" class="btn btn-primary btn-sm mb-3 mt-3">
                </div>
            </div>
        </form>
    </div>
 
    <div class="col-md-12">
        <?php if(!empty($_GET)): ?>
        <a href="<?= base_url('admin/laporan-penjualan/prints?tgl_a='.$tgl_a.'&tgl_b='.$tgl_b) ?>" class="btn btn-success mb-3 btn-sm mt-3" style="float:right" target="_BLANK">Print Laporan</a>
            <table class="table table-pemesanan" id="myTable" style="font-size: .8em">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tgl Pemesanan</th>
                        <th>Kode Pemesanan</th>
                        <th>Nama Pelanggan</th>
                        <th>Produk</th>
                        <th>Format Ukuran</th>
                        <th>Sisi Cetak</th>
                        <th>Qty</th>
                        <th>Jenis Kertas</th>
                        <th>Finishing</th>
                        <th>Total Pembayaran</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1; foreach($pemesanan as $data) { ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $data->tanggal ?></td>
                        <td><?= $data->id_pemesanan ?></td>
                        <td><?= $data->nama_pelanggan ?></td>
                        <td><?= $data->tipe_print ?></td>
                        <td><?= $data->format ?></td>
                        <td><?= $data->sisi_cetak ?></td>
                        <td><?= $data->quantity ?></td>
                        <td><?= $data->jenis_kertas ?></td>
                        <td><?= $data->finishing ?></td>
                        <td>Rp. <?= number_format($data->total_biaya) ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            
        <?php endif ?>
    </div>
</div>