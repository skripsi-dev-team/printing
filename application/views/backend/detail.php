    <div class="row">
        <div class="col-md-12">
            <p>Pemesan: <span class="badge badge-info"><?= $pemesanan->nama_pelanggan ?></span>. Tanggal <?= $pemesanan->tanggal ?>. </p>
            <p>Total Pesanan: <span class="badge badge-secondary"><?= number_format($pemesanan->total_biaya) ?></span></p>
            <div class="card-pesanan mb-4">
                <div class="left">
                    <p>Sisi depan</p>
                    <img class="card-img-left img-fluid" src="<?= base_url('assets/upload/'.$pemesanan->file_pesanan) ?>">

                    <?php 
                        if($pemesanan->file_pesanan_2 != null):
                    ?>
                    <hr>
                    <p>Sisi belakang</p>
                    <img class="card-img-left img-fluid" src="<?= base_url('assets/upload/'.$pemesanan->file_pesanan_2) ?>">
                    <?php endif ?>
                </div>
                <div class="center">
                    <h4><?= ucwords($pemesanan->tipe_print) ?></h4>
                    <ul>
                        <li>Ukuran <?= $pemesanan->format ?></li>
                        <li>Sisi cetak : <?= $pemesanan->sisi_cetak ?> Sisi</li>
                        <li>Kertas <?= $pemesanan->jenis_kertas ?></li>
                        <li><?= $pemesanan->quantity ?> pcs</li>
                        <?php if($pemesanan->finishing != null ) : ?>
                            <li>Finishing <?= $pemesanan->finishing ?></li>
                        <?php endif ?>
                    </ul>
                </div>
                <div class="right">
                   
                </div>
            </div>
        </div>
    </div>
    <?php if($this->session->jabatan == 1) : ?>
    <div class="row">        
        <div class="col-md-12">
            <h4>Data Pembayaran</h4>    
            <table class="table">
                <tr>
                    <th>Tanggal</th>
                    <th>Ongkir</th>
                    <th>Total Pembayaran</th>
                    <th>Bukti transfer</th>
                </tr>
                <?php if($pembayaran !== null)  :?>
                <tr>
                    <td><?= $pembayaran->tanggal ?></td>
                    <td><?= ($pembayaran->ongkir == 1) ? 'Ya (dikirim ke alamat)' : 'Tidak (diambil ke toko)' ?></td>
                    <td><?= number_format($pembayaran->nominal_pembayaran) ?></td>
                    <td><a href="<?= base_url('assets/upload/'.$pembayaran->bukti_transfer) ?>" target="_blank" class="text-primary">Periksa</a></td>
                </tr>
                <?php else : ?>
                <tr>
                    <td colspan="4" class="text-danger text-center">Pelanggan belum melakukan pembayaran</td>
                </tr>
                <?php endif ?>
            </table>

            <?php if($pembayaran !== null)  :?>
            <div class="admin-address mb-3 ml-2">
                <?php if($pembayaran->keterangan != '') : ?>
                    <p>Alamat pengiriman: <br>
                    <i><?= $pembayaran->keterangan ?></i></p>
                <?php else : ?>
                    <p>diambil ketoko</p>
                <?php endif ?>
                               
            </div>
            <?php endif ?>

        </div>
    </div>
    <?php endif ?>

    <div class="row mb-5">
        <div class="col-md-6">
            <h4>Status Pemesanan</h4>
            <form action="<?= base_url('admin/adminpemesanan/updatestatus/'.$pemesanan->id_pemesanan) ?>" method="post">
                <select name="status_pemesanan" class="form-control">
                    <?php for($i = 1; $i <= 6; $i++) : ?>
                    <option value="<?= $i ?>" <?= ($i == $pemesanan->status) ? 'selected' : '' ?> 
                            <?php if($pembayaran == null) { 
                                if($i != 6 && $i != 1 ) { 
                                    echo 'style="display:none"'; 
                                    } 
                                }
                                if($this->session->jabatan == 1 && $pembayaran != null) {
                                    if($i == 4 || $i == 5 || $i == 6){
                                        echo 'style="display:none"';
                                    }
                                } else if($this->session->jabatan == 2 && $pembayaran != null) {
                                    if($i == 1 || $i == 2 || $i == 3 || $i == 6 || $i==5){
                                        echo 'style="display:none"';
                                    } 
                                } 

                                ?> >
                                <?= statusPemesanan()[$i] ?></option>

                    <?php endfor ?>

                    <?php if($this->session->jabatan == 2 && $pemesanan->status == 4) { ?>
                        <option value="5">Selesai</option>
                    <?php } ?>

                </select>
                <br>
                <input type="submit" class="btn btn-sm btn-outline-success float-right" value="Update Pemesanan">
            </form>
            <?php if($pemesanan->status >= 3 && $this->session->jabatan == 1): ?>
                <a href="<?= base_url('admin/pemesanan/prints/'.$pemesanan->id_pemesanan) ?>" class="btn btn-sm btn-outline-success float-right mr-2" target="_blank"><i class="fas fa-print"></i> Cetak Nota</a>
            <?php endif ?>
        </div>
    </div>