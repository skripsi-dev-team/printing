<div class="container">
    <div class="login">
        <div class="kotak">
            <!-- flash message -->
            <?php $this->load->view('backend/part/flash.php') ?>
            <h3>Login User</h3>
            <hr>
            <form action="<?= site_url('login/postlogin') ?>" method="POST">
                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="Username">
                    <small class="error"><?php echo form_error('username') ?></small>
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <small class="error"><?php echo form_error('password') ?></small>
                </div>
                <div class="form-group" style="text-align:right">
                    <input type="submit" value="Login" class="btn btn-primary">
                </div>
            </form>
            <p style="text-align: right">
                <a href="<?= site_url('register') ?>">Daftar User</a>
            </p>
        </div>
    </div>
</div>