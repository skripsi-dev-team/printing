<div class="content" style="margin-top: 4%">
    <div class="container">
        <h3><i class="fas fa-map"></i> Informasi Kertas</h3>
        <div class="row">
            <div class="col-md-3">
                <ul class="list-group">
                    <?php foreach($kertasAll as $row): ?>
                        <li class="list-group-item <?= ($row['id'] == $id)? 'active' : '' ?>">
                            <a href="<?= site_url('home/detail/'.$row['id']) ?>">
                                <?= $row['nama'] ?>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="col-md-9 text-center">
                <div class="container">
                    <img src="<?= base_url('assets/img/'.$kertas['foto']) ?>" alt="img-kertas" width="30%"><br>
                    <b><?= $kertas['nama'] ?></b>
                    <p>
                        <?= $kertas['keterangan'] ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>