<div class="jumbotron">
    <div class="wreapper">
        <h1 class="display-4">Selamat Datang</h1>
        <p class="lead">Di sistem pemesanan</p>
        <h1 class="display-4">CV. GARDA TEGAK PEDIDI</h1>
    </div>
</div>
<div class="content">
    <div class="container">
        <h3><i class="fas fa-map"></i> Informasi Kertas</h3>
        <div class="row justify-content-md-center">
            <?php foreach($kertas as $row): ?>
                <div class="col-md-2 text-center">
                    <a href="<?= site_url('home/detail/'.$row['id']) ?>">
                        <img src="<?= base_url('assets/img/'.$row['foto']) ?>" alt="img-kertas" width="90%"><br>
                        <?= $row['nama'] ?>
                    </a>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>