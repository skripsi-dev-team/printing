<div class="container" style="margin-top: 10%; margin-bottom:10%">
    <div class="row">
        <div class="col-md-12">
            <h3>Data Pesanan</h3>
            <div class="card-pesanan mb-4">
                <div class="left">
                    <p>Sisi depan</p>
                    <img class="card-img-left img-fluid" src="<?= base_url('assets/upload/'.$pemesanan->file_pesanan) ?>">

                    <?php 
                        if($pemesanan->file_pesanan_2 != null):
                    ?>
                    <hr>
                    <p>Sisi belakang</p>
                    <img class="card-img-left img-fluid" src="<?= base_url('assets/upload/'.$pemesanan->file_pesanan_2) ?>">
                    <?php endif ?>
                </div>
                <div class="center">
                    <h4><?= ucwords($pemesanan->tipe_print) ?></h4>
                    <ul>
                        <li>Ukuran <?= $pemesanan->format ?></li>
                        <li>Sisi cetak : <?= $pemesanan->sisi_cetak ?> Sisi</li>
                        <li>Kertas <?= $pemesanan->jenis_kertas ?></li>
                        <li><?= $pemesanan->quantity ?> pcs</li>
                        <?php if($pemesanan->finishing != null ) : ?>
                            <li>Finishing <?= $pemesanan->finishing ?></li>
                        <?php endif ?>
                    </ul>
                </div>
                <div class="right">
                   
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <!-- flash message -->
            <?php $this->load->view('backend/part/flash.php') ?>
            <h3>Form Pembayaran</h3>
            <form action="<?= base_url('pembayaran/bayar/'.$pemesanan->id_pemesanan) ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label>Ongkir</label>
                    <select name="ongkir" class="form-control" id="ongkir" required>
                        <option value="0"> Tidak (diambil ke toko) </option>
                        <option value="1">Ya (dikirim ke alamat) </option>
                    </select>
                </div>

                <div id="include-ongkir" style="display:none">
                    <div class="form-group">
                        <label>Kabupaten <br><small class="text-danger">*Selain kota denpasar harga ongkir diluar sistem dan akan segera diinfo kan oleh staff</small></label>
                        <select name="kabupaten" class="form-control" id="kabupaten">
                            <option value="">- Pilih -</option>
                            <?php foreach(getKabupaten() as $kabupaten) : ?>
                                <option value="<?= $kabupaten ?>"><?= $kabupaten ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Alamat Lengkap</label>
                        <textarea name="keterangan" class="form-control" id="keterangan"></textarea>
                    </div>                    
                </div>


                <div class="form-group">
                    <label>Total yang harus dibayar</label>
                    <input type="text" readonly name="nominal_pembayaran" required class="form-control" value="<?= $pemesanan->total_biaya ?>" id="nominal_pembayaran">
                    <input type="hidden" name="default_harga" id="default_harga" value="<?= $pemesanan->total_biaya ?>">
                </div>
                <div class="form-group">
                    <label>Upload bukti transfer</label>
                    <input type="file" class="form-control" name="bukti_transfer" required>
                </div>
                <input type="submit" class="btn btn-primary float-right" value="Bayar Sekarang">
            </form>
        </div>
        <div class="col-md-6">
            <h3>Transfer ke</h3>
            <p><img src="<?= base_url('assets/img/logo-bni.png') ?>" alt="logo bni" class="img-fluid"></p>
            <p>Nomor Rekening: <strong>900017482382</strong> </p>
            <p>a/n GARDA PRINTING</p>

        </div>
    </div>
</div>