<div class="container" style="margin-top: 13%; margin-bottom:10%">
    <div class="row">
        <div class="col-md-12">
            <!-- flash message -->
            <?php $this->load->view('backend/part/flash.php') ?>
            <a href="<?= base_url('produk') ?>">< Pesan Kembali</a>
            <hr>
            <h3>Pesanan Saya</h3>
            <br>
            <?php foreach($pesanan as $row): ?>
            <div class="card-pesanan mb-4">
                <div class="left">
                    <img class="card-img-left img-fluid" src="<?= base_url('assets/upload/'.$row->file_pesanan) ?>">
                </div>
                <div class="center">
                    <h4><?= ucwords($row->tipe_print) ?></h4>
                    <ul>
                        <li>Ukuran <?= $row->format ?></li>
                        <li>Sisi cetak : <?= $row->sisi_cetak ?> Sisi</li>
                        <li>Kertas <?= $row->jenis_kertas ?></li>
                        <li><?= $row->quantity ?> pcs</li>
                        <?php if($row->finishing != null ) : ?>
                            <li>Finishing <?= $row->finishing ?></li>
                        <?php endif ?>
                    </ul>
                </div>
                <div class="right">
                    <p>Status Pemesanan : <br> <?= setStatus($row->status) ?></p>
                    <?php if($row->status == 1) : ?>
                    <div>
                        <a href="<?= base_url('pembayaran/'.$row->id_pemesanan) ?>" class="btn btn-success">Bayar Pesanan</a>
                    </div>
                    <?php endif ?>
                    <h5>Total : <?= number_format($row->total_biaya) ?></h5>
                    <small>(Belum ongkir)</small>
                </div>
            </div>
            <?php endforeach ?>
           
        </div>
    </div>
</div>