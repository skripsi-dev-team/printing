<div class="content" style="margin-top: 4%">
    <div class="container">
        <!-- flash message -->
        <?php $this->load->view('backend/part/flash.php') ?>
        
        <h3><i class="fas fa-map"></i> <?= $title ?></h3>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title"><strong>Harap isi seluruh data pada form dibawah.</strong></div>
                        <div class="card-content">
                            <form action="<?= base_url('pelangganregister/register') ?>" method="post">
                                <div class="form-group">
                                    <label>Nama Lengkap</label>
                                    <input type="text" name="nama_pelanggan" class="form-control" placeholder="Masukan nama pelanggan" value="<?= $input->nama_pelanggan ?>">
                                    <small class="error"><?php echo form_error('nama_pelanggan') ?></small>
                                </div>
                                <div class="form-group">
                                    <label>No Telp</label>
                                    <input type="number" name="no_telp" class="form-control" placeholder="Masukan no telp pelanggan" value="<?= $input->no_telp ?>">
                                    <small class="error"><?php echo form_error('no_telp') ?></small>
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea name="alamat" class="form-control" placeholder="Masukan alamat pelanggan"><?= $input->alamat ?></textarea>
                                    <small class="error"><?php echo form_error('alamat') ?></small>
                                </div>
                                
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" name="username" class="form-control" placeholder="Masukan username pelanggan" value="<?= $input->username ?>">
                                    <small class="error"><?php echo form_error('username') ?></small>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control" placeholder="Masukan password pelanggan">
                                    <small class="error"><?php echo form_error('password') ?></small>
                                </div>
                                <div class="form-group">
                                    <label>Retype Password</label>
                                    <input type="password" name="retype_password" class="form-control" placeholder="Masukan password ulang">
                                    <small class="error"><?php echo form_error('retype_password') ?></small>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" class="form-control" placeholder="Masukan email pelanggan" value="<?= $input->email ?>">
                                    <small class="error"><?php echo form_error('email') ?></small>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-success float-right btn-lg" value="Daftar Sekarang">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
