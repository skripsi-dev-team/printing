<!-- jquery -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

<!-- Popper.JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>

<!-- Bootstrap JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

<!-- SweetAlert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- custom script -->
<script>
	$(document).ready(function () {
	$('#kartu_nama').change(function () {
		var total = '';
		var sisi = $('#sisi_cetak').val();
		var qty = $('#qty').val();
		var kertas = $("#kertas option:selected").data('harga');
		var finishing = $("#finishing option:selected").data('harga');

		total = qty * (kertas + (sisi * 10000) + finishing);
		$('#total_biaya').val(total);
	});

	$('#brosur').change(function () {
		var format;
		var total = '';
		var sisi = $('#sisi_cetak').val();
		var qty = $('#qty').val();
		var panjang, lebar;

		if($('#format').prop('disabled')){

			$('.panjang').keyup(function(){
				panjang = this.value;
				$('.lebar').keyup(function(){
					lebar = this.value;

					format = parseInt(panjang) + parseInt(lebar);
				});
				
			});

			format = parseInt($('.panjang').val()) * parseInt($('.lebar').val()) * 5;
			
		} else {
			format = $("#format option:selected").data('harga');
		}

		var kertas = $("#kertas option:selected").data('harga');

		total = qty * (format + (sisi * 1000) + kertas);

		//console.log(total);
		$('#total_biaya').val(total);
	});

	$('#stiker').change(function () {
		var total = '';
		var qty = $('#qty').val();
		var kertas = $("#kertas option:selected").data('harga');
		var format;
		
		if($('#format').prop('disabled')){

			$('.panjang').keyup(function(){
				panjang = this.value;
				$('.lebar').keyup(function(){
					lebar = this.value;

					format = parseInt(panjang) + parseInt(lebar);
				});
				
			});

			format = parseInt($('.panjang').val()) * parseInt($('.lebar').val()) * 5;
			
		} else {
			format = $("#format option:selected").data('harga');
		}

		total = qty * (format + kertas);
		console.log(total);
		$('#total_biaya').val(total);
	});
});
</script>