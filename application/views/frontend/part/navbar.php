<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="<?= site_url('home') ?>">
            <img src="<?= base_url('assets/img/brand.jpg') ?>" width="110" height="auto">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link <?= ($this->uri->segment(1) == 'produk') ? 'active' : '' ?>" href="<?= site_url('produk') ?>"><i class="fas fa-archive"></i> PRODUK</a>
            </div>

            <div class="navbar-nav ml-auto">
                <?php if($this->session->userdata('login')): ?>
                    <div class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user"></i> <?= $this->session->userdata('nama_pelanggan') ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="<?= base_url('pemesanan') ?>"><i class="fas fa-shopping-cart"></i> Pesanan Saya</a>
                            <a class="dropdown-item" href="<?= site_url('login/logout') ?>"><i class="fas fa-power-off"></i> Logout</a>
                        </div>
                    </div>
                <?php else: ?>
                    <a class="nav-item nav-link" href="<?= site_url('register') ?>"><i class="fas fa-user"></i> Register</a> &nbsp;&nbsp;
                    <a class="nav-item nav-link btn btn-primary text-white" href="<?= site_url('login') ?>"><i class="fas fa-user"></i> Login</a>
                <?php endif ?>
            </div>
        </div>
    </div>
</nav>