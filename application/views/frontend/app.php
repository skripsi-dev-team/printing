<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('frontend/part/head.php') ?>
    <link rel="stylesheet" href="<?= base_url('assets/css/main.css') ?>">
</head>

<body> 
    <!-- navbar -->
    <?php $this->load->view('frontend/part/navbar.php') ?>

    <!-- content -->
    <?php $this->load->view('frontend/'.$content)  ?>
    <!-- content -->

    <!-- footer -->
    <?php $this->load->view('frontend/part/footer.php') ?>

    <!-- JS -->
    <?php $this->load->view('frontend/part/javascript.php') ?>

    <script>
        $('#ongkir').on('change', function(){
            var nominal = $('#nominal_pembayaran');
            var def_harga = $('#default_harga').val();
            if(this.value == 0){
                $('#include-ongkir').hide();
                nominal.val(def_harga);        
            } else {
                $('#include-ongkir').show();
        
                $('#kabupaten').on('change', function(){
                    if(this.value == 'Denpasar'){
                        nominal.val(parseInt(def_harga) + 10000);
                    } else {
                        nominal.val(def_harga);
                    }
                })
                
            }
        });
    </script>

    <script>
        function formCustom(){
            if($('#custom_check').prop('checked')){
                $('#format').prop('disabled', 'disabled');
                $('#custom_format').show();
            } else {
                $('#format').prop('disabled', '');
                $('#custom_format').hide();
            }
        }

        
        $(document).ready(function(){

            $('#sisi_cetak').on('change', function(){
                if(this.value == 2) {
                    $('#file_2').show();
                } else {
                    $('#file_2').hide();
                }
            });
        });
    </script>
</body>

</html>