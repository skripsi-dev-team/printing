<div class="content mt-5">
    <div class="container">
        <div class="row justify-content-md-center">
            <?php for ($i = 0; $i < count($product['produk']); $i++) { ?>
            <div class="produk">
                <div class="wrapper text-center">
                    <img src="<?= base_url('assets/img/'.$product['produk'][$i]['foto']) ?>" width="50%" alt="" class="img-round"> <br>
                    <b><?= ucwords($product['produk'][$i]['nama']) ?></b> <br>
                    <a href="<?= base_url('/produk/form/'.str_replace(' ', '-', $product['produk'][$i]['nama'])) ?>"><i class="fas fa-plus-circle"></i></a>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>