<div class="container" style="margin-top: 10%">
    <div class="row justify-content-md-center">
        <div class="col-md-8">
            <!-- flash message -->
            <?php $this->load->view('backend/part/flash.php') ?>
            <h3><?= $title." ".$produk['nama'] ?></h3>
            <hr>
            <form action="<?= site_url('pemesanan/storeKartuNama') ?>" method="POST" enctype="multipart/form-data" id="kartu_nama">
                <input type="hidden" name="tipe_print" value="kartu nama">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="format">Format</label>
                            <input type="text" name="format" class="form-control" value="9 x 5,5 cm" readonly>
                            <small class="error"><?php echo form_error('format') ?></small>
                        </div>
                        <div class="form-group">
                            <label for="sisi_cetak">Sisi Cetak</label>
                            <select name="sisi_cetak" id="sisi_cetak" class="form-control">
                                <option value="1">Depan</option>
                                <option value="2">Depan & Belakang</option>
                            </select>
                            <small class="error"><?php echo form_error('sisi_cetak') ?></small>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Quantity <span>(box)</span></label>
                            <div class="input-group">
                                <input type="number" name="quantity" id="qty" class="form-control">
                                <div class="input-group-append">
                                    <span class="input-group-text">Box</span>
                                </div>
                            </div>
                            <p class="text-danger">*1 box 100pcs</p>
                            <small class="error"><?php echo form_error('quantity') ?></small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_kertas">Jenis Kertas</label>
                            <select name="jenis_kertas" class="form-control" id="kertas">
                                <?php foreach($produk['jenis_kertas'] as $data => $n): ?>
                                <option value="<?= $n['nama_kertas'] ?>" data-harga="<?= $n['harga'] ?>"><?= $n['nama_kertas'] ?></option>
                                <?php endforeach ?>
                            </select>
                            <small class="error"><?php echo form_error('jenis_kertas') ?></small>
                        </div>
                        <div class="form-group">
                            <label for="finishing">Finishing</label>
                            <select name="finishing" id="finishing" class="form-control">
                                <?php foreach($produk['laminasi'] as $data => $n): ?>
                                <option value="<?= $n['nama_laminasi'] ?>" data-harga="<?= $n['harga'] ?>"><?= $n['nama_laminasi'] ?></option>
                                <?php endforeach ?>
                            </select>
                            <small class="error"><?php echo form_error('finishing') ?></small>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="total_biaya">Total Biaya (Rp)</label>
                    <input type="text" name="total_biaya" id="total_biaya" class="form-control" readonly>
                    <small class="error"><?php echo form_error('total_biaya') ?></small>
                </div>
                <div class="form-group">
                    <label for="file">Masukkan File (Depan)</label>
                    <input type="file" name="file_pesanan[]" class="form-control">
                    <small class="error"><?php echo form_error('file_pesanan') ?></small>
                </div>

                <div class="form-group" id="file_2" style="display: none">
                    <label for="file">Masukkan File (Belakang)</label>
                    <input type="file" name="file_pesanan[]" class="form-control">
                    <small class="error"><?php echo form_error('file_pesanan') ?></small>
                </div>
                <div class="form-group" style="text-align:right">
                    <input type="submit" value="Simpan Pesanan" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>