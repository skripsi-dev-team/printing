<div class="container" style="margin-top: 10%">
    <div class="row justify-content-md-center">
        <div class="col-md-8">
            <!-- flash message -->
            <?php $this->load->view('backend/part/flash.php') ?>
            <h3><?= $title." ".$produk['nama'] ?></h3>
            <hr>
            <form action="<?= site_url('pemesanan/storeStiker') ?>" method="POST" enctype="multipart/form-data" id="stiker">
                <input type="hidden" name="tipe_print" value="stiker">    
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="format">Format <small><input type="checkbox" id="custom_check" onclick="formCustom()"> custom ukuran?</small> </label>
                            <select name="format" id="format" class="form-control">
                                <?php foreach($produk['format'] as $data => $n): ?>
                                <option value="<?= $n['ukuran'] ?>" data-harga="<?= $n['harga'] ?>"><?= $n['ukuran'] ?></option>
                                <?php endforeach ?>
                            </select>

                            <div class="row mt-4" id="custom_format" style="display: none">
                                <div class="col-md-5">
                                    <input type="number" name="panjang" class="panjang form-control" value="1">
                                </div>
                                <div class="mt-2">
                                    X    
                                </div>
                                
                                <div class="col-md-5">
                                    <input type="number" name="lebar" class="lebar form-control" value="1">
                                </div>
                                <div class="mt-2">
                                    CM
                                </div>                                
                            </div>
                            

                            <small class="error"><?php echo form_error('format') ?></small>
                        </div>
                        <div class="form-group">
                            <label for="sisi_cetak">Sisi Cetak</label>
                            <select name="sisi_cetak" id="sisi_cetak" class="form-control">
                                <option value="1">1</option>
                            </select>
                            <small class="error"><?php echo form_error('sisi_cetak') ?></small>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jenis_kertas">Jenis Kertas</label>
                            <select name="jenis_kertas" class="form-control" id="kertas">
                                <?php foreach($produk['jenis_kertas'] as $data => $n): ?>
                                <option value="<?= $n['nama_kertas'] ?>" data-harga="<?= $n['harga'] ?>"><?= $n['nama_kertas'] ?></option>
                                <?php endforeach ?>
                            </select>
                            <small class="error"><?php echo form_error('jenis_kertas') ?></small>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Quantity <span>(pcs)</span></label>
                            <input type="number" name="quantity" id="qty" class="form-control" min="50">
                            <p class="text-danger">*Min 50 pcs</p>
                            <small class="error"><?php echo form_error('quantity') ?></small>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="total_biaya">Total Biaya (Rp)</label>
                    <input type="text" name="total_biaya" id="total_biaya" class="form-control" readonly>
                    <small class="error"><?php echo form_error('total_biaya') ?></small>
                </div>
                <div class="form-group">
                    <label for="file">Masukkan File</label>
                    <input type="file" name="file_pesanan[]" class="form-control">
                    <small class="error"><?php echo form_error('file_pesanan') ?></small>
                </div>
                <div class="form-group" style="text-align:right">
                    <input type="submit" value="Simpan Pesanan" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>