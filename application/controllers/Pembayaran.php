<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller {


    public function __construct(){
        parent::__construct();
        $this->load->model('pemesanan_model', 'pemesanan');
        $this->load->model('pembayaran_model', 'pembayaran');
        // cek login
        checkPelanggan();
    }

    public function index($id_pemesanan){
        $data['title'] = "Bayar Pesanan";
        $data['content'] = "pesanan/pembayaran";
        $data['pemesanan'] = $this->pemesanan->find($id_pemesanan);

        $this->load->view('frontend/app', $data);
    }

    public function bayar($id_pemesanan){
        //simpan pembayaran
        $this->pembayaran->store($id_pemesanan);
        //update status pemesanan
        $this->db->set('status', 2)->where('id_pemesanan', $id_pemesanan)->update('pemesanan');

        //notif bayar ke kasir

        $pemesanan = $this->pemesanan->find($id_pemesanan);

        $this->db->insert('notifikasi', [
            'tanggal' => date('Y-m-d H:i:s'),
            'judul' => 'Pembayaran '. ucwords($pemesanan->tipe_print),
            'isi' => 'Atas nama '. $this->session->userdata('nama_pelanggan') .' baru saja melakukan pembayaran, silahkan dicek bukti transfer dan segera proses pemesanan.',
            'untuk' => 1,
            'dilihat' => 0,
        ]);

        $this->session->set_flashdata('success', 'Pembayaran Berhasil dilakukan');
        redirect(base_url('pemesanan'));
    }
}