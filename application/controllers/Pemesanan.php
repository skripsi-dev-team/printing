<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemesanan extends CI_Controller {

    private $model = 'pemesanan';
    public function __construct(){
        parent::__construct();
        $this->load->model($this->model.'_model', $this->model);
        
        // cek login
        checkPelanggan();
    }

    public function index(){
        $data['title'] = 'Pesanan Saya';
        $data['content'] = 'pesanan/pesanan';
        $data['pesanan'] = $this->pemesanan->pesanan($this->session->userdata('id_pelanggan'));
        $data['no'] = 1;

        $this->load->view('frontend/app', $data);
    }

    public function storeKartuNama(){
        $rules = $this->pemesanan->rulesKartuNama();
        $validation = $this->form_validation;
        $validation->set_rules($rules);

        if($validation->run()){
            $this->pemesanan->storePemesanan();
            //notif ke kasir
            $this->db->insert('notifikasi', [
                'tanggal' => date('Y-m-d H:i:s'),
                'judul' => 'Pemesanan Kartu Nama Baru',
                'isi' => 'Atas nama '. $this->session->userdata('nama_pelanggan') .' dengan status menunggu pembayaran',
                'untuk' => 1,
                'dilihat' => 0,
            ]);

            $this->session->set_flashdata('success', 'Pesanan Berhasil dilakukan');
            redirect(base_url('pemesanan'));
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function storeBrosur(){
       
        $rules = $this->pemesanan->rulesBrosur();
        $validation = $this->form_validation;
        $validation->set_rules($rules);

        if($validation->run()){
            $this->pemesanan->storePemesanan();
            //notif ke kasir
            $this->db->insert('notifikasi', [
                'tanggal' => date('Y-m-d H:i:s'),
                'judul' => 'Pemesanan Brosur Baru',
                'isi' => 'Atas nama '. $this->session->userdata('nama_pelanggan') .' dengan status menunggu pembayaran',
                'untuk' => 1,
                'dilihat' => 0,
            ]);


            $this->session->set_flashdata('success', 'Pesanan Berhasil dilakukan');
            redirect(base_url('pemesanan'));
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function storeStiker(){
        $rules = $this->pemesanan->rulesStiker();
        $validation = $this->form_validation;
        $validation->set_rules($rules);

        if($validation->run()){
            $this->pemesanan->storePemesanan();

            //notif ke kasir
            $this->db->insert('notifikasi', [
                'tanggal' => date('Y-m-d H:i:s'),
                'judul' => 'Pemesanan Stiker Baru',
                'isi' => 'Atas nama '. $this->session->userdata('nama_pelanggan') .' dengan status menunggu pembayaran',
                'untuk' => 1,
                'dilihat' => 0,
            ]);


            $this->session->set_flashdata('success', 'Pesanan Berhasil dilakukan');
            redirect(base_url('pemesanan'));
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
}