<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct(){
        parent::__construct();
       
    }

    public function index(){
        $data['title'] = 'Home';
        $data['content'] = 'home/index';
        $data['kertas'] = $this->paper();
        
        $this->load->view('frontend/app', $data);
    }

    public function detail($id){
        $data['title'] = 'Detail';
        $data['content'] = 'home/detail';
        $data['kertasAll'] = $this->paper();
        $data['kertas'] = $this->paper($id);
        $data['id'] = $id;

        $this->load->view('frontend/app', $data);
    }

    function paper($id = null){
        $file = base_url('assets/json/paper.json');
        $paper = file_get_contents($file);
        $paper = json_decode($paper, TRUE);

        if(isset($id)){
            foreach($paper as $data){
                if($data['id'] == $id){
                    $paper = $data;
                }
            }
        }

        return $paper;
    }
}