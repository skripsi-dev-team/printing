<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

    public function __construct(){
        parent::__construct();
       
    }

    public function index(){
        $data['title'] = 'Produk';
        $data['content'] = 'produk/index';
        $data['product'] = $this->getProduct();

        $this->load->view('frontend/app', $data);
    }

    public function form($nama){
        checkPelanggan();
        
        $data['title'] = 'Pemesanan';
        $data['produk'] = $this->getProduct(removeMIn($nama));
        
        // page
        switch ($nama) {
            case 'kartu-nama':
                $data['content'] = 'produk/form/kartu_nama';
                break;

            case 'brosur':
                $data['content'] = 'produk/form/brosur';
                break;
            
            default:
                $data['content'] = 'produk/form/stiker';
                break;
        }

        $this->load->view('frontend/app', $data);
    }

    public function getProduct($nama = null){
        $produk = '';
        $product = array('produk' => [
            0 => [
                'nama' => 'kartu nama',
                'jenis_kertas' => [
                    0 => [
                        'nama_kertas' => 'art carton',
                        'harga' => 10000
                    ],
                    1 => [
                        'nama_kertas' => 'matt paper',
                        'harga' => 10000
                    ]
                ],
                'laminasi' => [
                    0 => [
                        'nama_laminasi' => 'glossy',
                        'harga' => 10000,
                    ],
                    1 => [
                        'nama_laminasi' => 'dof',
                        'harga' => 10000
                    ]
                ],
                'foto' => 'kartu-nama.png'
            ],
            1 => [
                'nama' => 'brosur',
                'format' => [
                    0 => [
                        'nama_format' => 'A4',
                        'ukuran' => '21 x  29,7 cm',
                        'harga' => 1300, 
                    ],
                    1 => [
                        'nama_format' => 'A5',
                        'ukuran' => '21 x  14,8 cm',
                        'harga' => 800
                    ]
                ],
                'jenis_kertas' => [
                    0 => [
                        'nama_kertas' => 'art carton',
                        'harga' => 1500
                    ],
                    1 => [
                        'nama_kertas' => 'matt paper',
                        'harga' => 2000
                    ]
                ],
                'foto' => 'brosur.png'
            ],
            2 => [
                'nama' => 'stiker',
                'format' => [
                    0 => [
                        'ukuran' => '5 x 5 cm',
                        'harga' => 500, 
                    ],
                    1 => [
                        'ukuran' => '6 x 6 cm',
                        'harga' => 600
                    ],
                    2 => [
                        'ukuran' => '7 x 7 cm',
                        'harga' => 700
                    ]
                ],
                'jenis_kertas' => [
                    0 => [
                        'nama_kertas' => 'Stiker Glossy',
                        'harga' => 500
                    ],
                    1 => [
                        'nama_kertas' => 'Stiker Doff',
                        'harga' => 800
                    ]
                ],
                'foto' => 'stiker.png'
            ]
        ]);

        if(isset($nama)){
            for($i=0; $i<count($product['produk']); $i++){
                if($product['produk'][$i]['nama'] == $nama){
                    $produk = $product['produk'][$i];
                }
            }
        }else{
            $produk = $product;
        }

        return $produk;
    }
}