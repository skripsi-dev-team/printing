<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
    private $model = 'pelanggan';

    public function __construct(){
        parent::__construct();
        $this->load->model($this->model.'_model', $this->model);
        
    }

    public function formLoginPelanggan(){
        checkPelangganSession();
        $data['title'] = 'Login';
        $data['content'] = 'home/login';

        $this->load->view('frontend/app', $data);
    }

    public function postlogin(){
        $rules = $this->pelanggan->rulesLogin();
        $validation = $this->form_validation;
        $validation->set_rules($rules);

        if($validation->run()){
            if($this->pelanggan->login()){
                $this->session->set_flashdata('success', 'Login Berhasil!');
                redirect(base_url('home'));
            }else{
                $this->session->set_flashdata('error', 'Username dan password tidak terdaftar!');
                $this->formLoginPelanggan();
            }
        }else{
            $this->session->set_flashdata('error', 'Login Gagal');
            $this->formLoginPelanggan();
        }
    }

    public function logout(){
        $this->pelanggan->unsetSession();
        redirect('home');
    }
}