<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminPemesanan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pemesanan_model', 'pemesanan');
        $this->load->model('pembayaran_model', 'pembayaran');
    }
    
    public function pesananMasuk(){
        $data['title'] = "Data Pemesanan Masuk";
        $data['content'] = 'pemesanan/pemesanan_masuk';
        $data['no'] = 1;
        $data['pesanan'] = $this->pemesanan->allMasuk();
        
        $this->load->view('backend/app', $data);
    }

    public function pesananProses(){
        $data['title'] = "Data Pemesanan Diproses";
        $data['content'] = 'pemesanan';
        $data['no'] = 1;
        $data['pesanan'] = $this->pemesanan->allProses();

        $this->load->view('backend/app', $data);
    }

    public function pesananSelesai(){
        $data['title'] = "Data Pemesanan Selesai";
        $data['content'] = 'pemesanan';
        $data['no'] = 1;
        $data['pesanan'] = $this->pemesanan->allSelesai();

        $this->load->view('backend/app', $data);
    }

    public function pesananBatal(){
        $data['title'] = "Data Pemesanan Batal";
        $data['content'] = 'pemesanan/pesanan_batal';
        $data['no'] = 1;
        $data['pesanan'] = $this->pemesanan->allBatal();

        $this->load->view('backend/app', $data);
    }


    public function detail($id_pemesanan){
        $data['title'] = 'Detail Pemesanan';
        $data['content'] = 'detail';
        $data['pemesanan'] = $this->pemesanan->find($id_pemesanan);
        $data['pembayaran'] = $this->pembayaran->findPembayaran($id_pemesanan);
        $this->load->view('backend/app', $data);

    }

    public function updateStatus($id_pemesanan){
        $status = $this->input->post('status_pemesanan');
        //jika status 1, maka hapus pembayaran karena pembayaran dianggap gagal.
        if($status == 1){
            $this->db->where('id_pemesanan', $id_pemesanan)->delete('pembayaran');
        }

        $pemesanan = $this->pemesanan->find($id_pemesanan);

        //notif ke petugas
        if($status == 3){
            $this->db->insert('notifikasi', [
                'tanggal' => date('Y-m-d H:i:s'),
                'judul' => 'Pembayaran '. $pemesanan->nama_pelanggan . ' Berhasil',
                'isi' => 'Atas nama '. ucwords($pemesanan->nama_pelanggan) .' telah terkonfirmasi melakukan pembayaran, mohon untuk segera memproses pesanan tersebut.',
                'untuk' => 2,
                'dilihat' => 0,
            ]);
        }

        //notif ke kasir kalo pesanan lagi dikerjakan
        if($status == 4) {
            $this->db->insert('notifikasi', [
                'tanggal' => date('Y-m-d H:i:s'),
                'judul' => 'Pemesanan '. ucwords($pemesanan->tipe_print). ' Sedang Dikerjakan',
                'isi' => 'Atas nama '. ucwords($pemesanan->nama_pelanggan) .' pesanan sedang dikerjakan oleh petugas printing.',
                'untuk' => 1,
                'dilihat' => 0,
            ]);
        }

        //notif ke kasir kalo pemesanan udah selesai
        if($status == 5) {
            $this->db->insert('notifikasi', [
                'tanggal' => date('Y-m-d H:i:s'),
                'judul' => 'Pemesanan '. ucwords($pemesanan->tipe_print). ' Selesai Dikerjakan',
                'isi' => 'Atas nama '. ucwords($pemesanan->nama_pelanggan) .' telah selesai dikerjakan dan barang sudah siap diambil atau dikirim ke pelanggan.',
                'untuk' => 1,
                'dilihat' => 0,
            ]);
        }
        $this->db->set('status', $status)->where('id_pemesanan', $id_pemesanan)->update('pemesanan');
        $this->session->set_flashdata('success', 'Pesanan Berhasil diupdate');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function prints($id_pemesanan){
        $data['pemesanan'] = $this->pemesanan->find($id_pemesanan);
        $data['pembayaran'] = $this->pembayaran->findPembayaran($id_pemesanan);
        $this->load->library('pdf');

        $this->pdf->setPaper('A5', 'landscape');
        $this->pdf->filename = "nota-penjualan.pdf";

        $this->pdf->load_view('backend/print.php', $data);
    }

}
