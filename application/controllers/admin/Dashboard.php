<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct(){
        parent::__construct();
        
        // ceklogin
        cekLogin();
    }
	
	public function index()
	{
		$today = date('Y-m-d');
		$data['title'] = "Selamat datang";
		$data['content'] = 'dashboard';
		$data['pesanan_masuk'] = $this->db->query("SELECT count(id_pemesanan) as masuk FROM pemesanan WHERE tanggal like '%$today%' ")->row();
		$data['pesanan_total'] = $this->db->query("SELECT count(id_pemesanan) as total FROM Pemesanan")->row();

		$grafik = $this->db->query("SELECT MONTH(tanggal) as bulan, COUNT(id_pemesanan) as totalPerbulan FROM pemesanan WHERE tanggal >= '2019-01-01' AND tanggal <= '2019-12-31' GROUP BY MONTH(tanggal)")->result();
	
		$month = [];
		$perBulan = [];
	
		foreach($grafik as $key => $value) {
			//$bulan .= $value->bulan . ', ';
			array_push($month, 'Bulan '. $value->bulan);
			array_push($perBulan, $value->totalPerbulan);
		}

		$data['month'] = json_encode($month);
		$data['perBulan'] = json_encode($perBulan);

		
		$this->load->view('backend/app', $data);
	}

	public function chart(){
		$grafik = $this->db->query("SELECT MONTH(tanggal) as bulan, COUNT(id_pemesanan) as totalPerbulan FROM pemesanan WHERE tanggal >= '2019-01-01' AND tanggal <= '2019-12-31' GROUP BY MONTH(tanggal)")->result();
	
		$month = [];
		$perBulan = [];
	
		foreach($grafik as $key => $value) {
			//$bulan .= $value->bulan . ', ';
			array_push($month, $value->bulan);
			array_push($perBulan, $value->totalPerbulan);
		}

		print_r(json_encode($month));
	}


	public function updateNotif($level){
		$q = $this->db->update('notifikasi', ['dilihat' => '1'], ['untuk' => $_GET['untuk']]);
		return $q;
	}
}
