<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	private $model = 'petugas';

    public function __construct(){
        parent::__construct();
        $this->load->model($this->model.'_model', $this->model);
	}
	
	public function index()
	{
		$data['title'] = "Admin";
		$this->load->view('backend/login', $data);
	}

	public function postlogin(){
        $rules = $this->petugas->rulesLogin();
        $validation = $this->form_validation;
        $validation->set_rules($rules);

        if($validation->run()){
            if($this->petugas->login()){
                $this->session->set_flashdata('success', 'Login Berhasil!');
                redirect(base_url('admin/dashboard'));
            }else{
                $this->session->set_flashdata('error', 'Username dan password tidak terdaftar!');
                $this->index();
            }
        }else{
            $this->session->set_flashdata('error', 'Login Gagal');
            $this->index();
        }
    }

    public function logout(){
        $this->petugas->unsetSession();
        redirect('admin');
    }
}
