<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class LaporanPenjualan extends CI_Controller {

    private $model = 'pemesanan';
    public function __construct(){
        parent::__construct();
        $this->load->model($this->model.'_model', $this->model);
        
        // ceklogin
        cekLogin();
    }

    public function index(){
        $data['title'] = "Laporan Penjualan";
        $data['content'] = 'laporan/laporan_penjualan';
        $data['pemesanan'] = $this->pemesanan->laporanAll();

        $this->load->view('backend/app', $data);
    }

    public function prints(){

        $data['pemesanan'] = $this->pemesanan->laporanAll();
        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "laporan-penjualan.pdf";

        $this->pdf->load_view('backend/laporan/print_penjualan.php', $data);
    }
}