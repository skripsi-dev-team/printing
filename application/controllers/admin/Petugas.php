<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas extends CI_Controller {

    private $model = "petugas";

    public function __construct(){
        parent::__construct();
        $this->load->model($this->model.'_model', $this->model);

        // ceklogin
        cekLogin();
    }

    public function index(){
        $data['petugas'] = $this->petugas->all();
        $data['title'] = "Data Petugas";
        $data['content'] = 'petugas/index';
        $this->load->view('backend/app', $data);
    }

    public function create(){
        if(!$_POST){
            $data['input'] = (object) $this->petugas->getDefaultValues();
        } else {
            $data['input'] = (object) $this->input->post();
        }
        $data['title'] = "Tambah Petugas";
        $data['content'] = 'petugas/form';
        $this->load->view('backend/app', $data);
    }

    public function store(){
        $rules = $this->petugas->rules();
        $validation = $this->form_validation;
        $validation->set_rules($rules);

        if($validation->run()){
            $this->petugas->insert();
            $this->session->set_flashdata('success', 'Data petugas disimpan');
            redirect(base_url('admin/petugas'));
        } else {
            $this->create();
        }
    }

    public function edit($id = null){
        if (!isset($id)) redirect('admin/petugas');

        $data['input'] = $this->petugas->find($id);
        
        $data['title'] = 'Edit Petugas';
        $data['content'] = 'petugas/form';

        $this->load->view('backend/app', $data);
    }

    public function update($id) {
        $rules = $this->petugas->rulesEdit();
        $validation = $this->form_validation;
        $validation->set_rules($rules);

        if($validation->run()){
            $this->petugas->update($id);
            $this->session->set_flashdata('success', 'Data petugas berhasil diedit');
            redirect(base_url('admin/petugas'));
        } else {
            $this->edit($id);
        }
    }


    public function delete($id){
        if (!isset($id)) show_404();
        $this->petugas->delete($id);
        $this->session->set_flashdata('success', 'Berhasil menghapus data petugas');
        redirect(base_url('admin/petugas'));
    }
}