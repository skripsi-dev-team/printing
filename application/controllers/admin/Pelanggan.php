<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends CI_Controller {

    private $model = "pelanggan";

    public function __construct(){
        parent::__construct();
        $this->load->model($this->model.'_model', $this->model);

        // ceklogin
        cekLogin();
    }

    public function index(){
        $data['title'] = "Data Pelanggan";
        $data['content'] = 'pelanggan/index';
        $data['pelanggan'] = $this->pelanggan->all();
        $this->load->view('backend/app', $data);
    }

    public function create() {
        if(!$_POST){
            $data['input'] = (object) $this->pelanggan->getDefaultValues();
        } else {
            $data['input'] = (object) $this->input->post();
        }
        $data['title'] = "Tambah Pelanggan";
        $data['content'] = 'pelanggan/form';
        $this->load->view('backend/app', $data);
    }

    public function store(){
        $rules = $this->pelanggan->rules();
        $validation = $this->form_validation;
        $validation->set_rules($rules);

        if($validation->run()){
            $this->pelanggan->insert();
            $this->session->set_flashdata('success', 'Data pelanggan disimpan');
            redirect(base_url('admin/pelanggan'));
        } else {
            $this->create();
        }
    }

    public function edit($id = null){
        if (!isset($id)) redirect('admin/petugas');

        $data['input'] = $this->pelanggan->find($id);
        
        $data['title'] = 'Edit pelanggan';
        $data['content'] = 'pelanggan/form';

        $this->load->view('backend/app', $data);
    }

    public function update($id) {
        $rules = $this->pelanggan->rulesEdit();
        $validation = $this->form_validation;
        $validation->set_rules($rules);

        if($validation->run()){
            $this->pelanggan->update($id);
            $this->session->set_flashdata('success', 'Data pelanggan berhasil diedit');
            redirect(base_url('admin/pelanggan'));
        } else {
            $this->edit($id);
        }
    }

    public function delete($id){
        if (!isset($id)) show_404();
        $this->pelanggan->delete($id);
        $this->session->set_flashdata('success', 'Berhasil menghapus data pelanggan');
        redirect(base_url('admin/pelanggan'));
    }
}