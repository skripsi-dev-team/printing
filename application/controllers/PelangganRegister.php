<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PelangganRegister extends CI_Controller {

    private $model = 'pelanggan';
    public function __construct(){
        parent::__construct();
        $this->load->model($this->model.'_model', $this->model);
        
    }

    public function index(){
        if(!$_POST){
            $data['input'] = (object) $this->pelanggan->getDefaultValues();
        } else {
            $data['input'] = (object) $this->input->post();
        }
        $data['content'] = 'register';
        $data['title'] = 'Registrasi Pelanggan';
        $this->load->view('frontend/app', $data);
    }

    public function register(){
        $rules = $this->pelanggan->rulesRegister();
        $validation = $this->form_validation;
        $validation->set_rules($rules);

        if($validation->run()){
            $this->pelanggan->insert();
            $this->session->set_flashdata('success', 'Registrasi berhasil! Silahkan login');
            redirect(base_url('login'));
        } else {
            $this->index();
        }
    }
}