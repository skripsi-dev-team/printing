<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran_model extends CI_Model {
    
    protected $table = 'pembayaran';

    public function all() {
        return $this->db->get($this->table)->result();
    }

    public function find($id) {
        return $this->db->get_where($this->table, ['id_pembayaran' => $id])->row();
    }

    public function findPembayaran($id) {
        return $this->db->get_where($this->table, ['id_pemesanan' => $id])->row();
    }

    public function store($id){
        $post = $this->input->post();
        
        $post['tanggal'] = date('Y-m-d H:i:s');
        $post['bukti_transfer'] = $this->uploadImage();
        $post['id_pemesanan'] = $id;
        $post['status_pembayaran'] = 1;

        if($post['keterangan'] == ''){
            $post['keterangan'] = "diambil ke toko";
        } else {
            $post['keterangan'] = $post['keterangan']. ", kabupaten " .$post['kabupaten'];
        }

        unset($post['default_harga']);
        unset($post['kabupaten']);

        $this->db->insert($this->table, $post);
    }

    private function uploadImage(){
        $config['upload_path']          = './assets/upload/';
        $config['allowed_types']        = 'jpg|png';
        $config['file_name']            = uniqid();
        $config['overwrite']			= true;
        $config['max_size']             = 3072; //3mb

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('bukti_transfer')) {
            return $this->upload->data("file_name");
        }else{
            $this->session->set_flashdata('error', 'File lebih dari 3mb');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
}