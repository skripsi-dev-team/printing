<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas_model extends CI_Model {
    
    protected $table = 'petugas';

    public function all(){
        return $this->db->get($this->table)->result();
    } 

    public function find($id){
        return $this->db->get_where($this->table, ['id_petugas' => $id])->row();
    }

    public function insert(){
        $post = $this->input->post();
        $post['password'] = sha1($post['password']);
        $this->db->insert($this->table, $post);
    }

    public function update($id){
        $post = $this->input->post();
        $this->db->update($this->table, $post, array('id_petugas' => $id));
    }

    public function rules(){
        return [
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|min_length[6]',
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|min_length[6]'
            ],
            [
                'field' => 'nama_petugas',
                'label' => 'Nama Petugas',
                'rules' => 'required'
            ],
            [
                'field' => 'no_telp',
                'label' => 'No Telp',
                'rules' => 'required|numeric'
            ],
            [
                'field' => 'jabatan',
                'label' => 'Jabatan',
                'rules' => 'required'
            ],
        ];
    }
    public function rulesEdit(){
        return [
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|min_length[6]',
            ],
            [
                'field' => 'nama_petugas',
                'label' => 'Nama Petugas',
                'rules' => 'required'
            ],
            [
                'field' => 'no_telp',
                'label' => 'No Telp',
                'rules' => 'required|numeric'
            ],
            [
                'field' => 'jabatan',
                'label' => 'Jabatan',
                'rules' => 'required'
            ],
        ];
    }

    public function rulesLogin(){
        return [
            [
                'field' => 'username',
                'label' => 'username',
                'rules' => 'required|min_length[6]'
            ],
            [
                'field' => 'password',
                'label' => 'password',
                'rules' => 'required|min_length[6]'
            ]
        ];
    }

    public function getDefaultValues(){
        return [
            'username' => '',
            'password' => '',
            'nama_petugas' => '',
            'no_telp' => '',
            'jabatan' => ''
        ];
    }

    public function delete($id)
    {
        return $this->db->delete($this->table, array("id_petugas" => $id));
    }

    public function login(){
        $post = $this->input->post();
        $post['password'] = sha1($post['password']);

        $data = $this->db->get_where($this->table, ['username' => $post['username'], 'password' => $post['password']])->row();

        if(!empty($data)){
            $this->setSession($data);
            return true;
        }else{
            return false;
        }
    }

    public function setSession($data){
        $array = array(
            'id_petugas' => $data->id_petugas,
            'nama_petugas'=> $data->nama_petugas,
            'jabatan' => $data->jabatan,
            'login_backend' => TRUE
        );

        $this->session->set_userdata($array);
    }

    public function unsetSession(){
        $array = array('id_petugas', 'nama_petugas', 'jabatan', 'login_backend');
        $this->session->unset_userdata($array);
    }
}