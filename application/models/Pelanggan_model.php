<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan_model extends CI_Model {
    
    protected $table = 'pelanggan';

    public function all() {
        return $this->db->get($this->table)->result();
    }

    public function find($id) {
        return $this->db->get_where($this->table, ['id_pelanggan' => $id])->row();
    }

    public function insert(){
        $post = $this->input->post();
        if(isset($post['retype_password'])){
            unset($post['retype_password']);
        }
        $post['password'] = sha1($post['password']);
        $this->db->insert($this->table, $post);
    }

    public function update($id){
        $post = $this->input->post();
        $this->db->update($this->table, $post, array('id_pelanggan' => $id));
    }

    public function rules(){
        return [
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|min_length[6]',
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|min_length[6]'
            ],
            [
                'field' => 'nama_pelanggan',
                'label' => 'Nama Pelanggan',
                'rules' => 'required'
            ],
            [
                'field' => 'no_telp',
                'label' => 'No Telp',
                'rules' => 'required|numeric'
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'required'
            ]
        ];
    }
    public function rulesEdit(){
        return [
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|min_length[6]',
            ],
            [
                'field' => 'nama_pelanggan',
                'label' => 'Nama Pelanggan',
                'rules' => 'required'
            ],
            [
                'field' => 'no_telp',
                'label' => 'No Telp',
                'rules' => 'required|numeric'
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'required'
            ]
        ];
    }

    public function rulesRegister(){
        return [
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|min_length[6]',
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|min_length[6]'
            ],
            [
                'field' => 'retype_password',
                'label' => 'Retype Password',
                'rules' => 'required|matches[password]'
            ],
            [
                'field' => 'nama_pelanggan',
                'label' => 'Nama Pelanggan',
                'rules' => 'required'
            ],
            [
                'field' => 'no_telp',
                'label' => 'No Telp',
                'rules' => 'required|numeric'
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'required'
            ]
        ];
    }

    public function rulesLogin(){
        return [
            [
                'field' => 'username',
                'label' => 'username',
                'rules' => 'required|min_length[6]'
            ],
            [
                'field' => 'password',
                'label' => 'password',
                'rules' => 'required|min_length[6]'
            ]
        ];
    }

    public function getDefaultValues(){
        return [
            'username' => '',
            'password' => '',
            'nama_pelanggan' => '',
            'no_telp' => '',
            'email' => '',
            'alamat' => ''
        ];
    }

    public function delete($id)
    {
        return $this->db->delete($this->table, array("id_pelanggan" => $id));
    }

    public function login(){
        $post = $this->input->post();
        $post['password'] = sha1($post['password']);

        $data = $this->db->get_where($this->table, ['username' => $post['username'], 'password' => $post['password']])->row();

        if(!empty($data)){
            $this->setSession($data);
            return true;
        }else{
            return false;
        }
    }

    public function setSession($data){
        $array = array(
            'id_pelanggan' => $data->id_pelanggan,
            'username' => $data->username,
            'nama_pelanggan'=> $data->nama_pelanggan,
            'login' => TRUE,
            'type' => 'member'
        );

        $this->session->set_userdata($array);
    }

    public function unsetSession(){
        $array = array('id_pelanggan', 'username', 'nama_pelanggan', 'login');
        $this->session->unset_userdata($array);
    }
}