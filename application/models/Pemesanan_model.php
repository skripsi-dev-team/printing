<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemesanan_model extends CI_Model {
    
    protected $table = 'pemesanan';
    
    public function all() {
        return $this->db->join('pelanggan', 'pelanggan.id_pelanggan = pemesanan.id_pelanggan')->order_by('tanggal', 'desc')->get($this->table)->result();
    }

    public function allMasuk() {
        return $this->db->query("SELECT * FROM pemesanan JOIN pelanggan ON pelanggan.id_pelanggan = pemesanan.id_pelanggan WHERE status in (1,2,3) ORDER BY tanggal = 'desc' ")->result();
        //return $this->db->join('pelanggan', 'pelanggan.id_pelanggan = pemesanan.id_pelanggan')->$this->whereIn('status', ['1','2'])->in('1','2')->order_by('tanggal', 'desc')->get($this->table)->result();
    }

    public function allProses(){
        return $this->db->query("SELECT * FROM pemesanan JOIN pelanggan ON pelanggan.id_pelanggan = pemesanan.id_pelanggan WHERE status in (3,4) ORDER BY tanggal = 'desc' ")->result();
    }

    public function allSelesai(){
        return $this->db->query("SELECT * FROM pemesanan JOIN pelanggan ON pelanggan.id_pelanggan = pemesanan.id_pelanggan WHERE status in (5) ORDER BY tanggal = 'desc' ")->result();
    }

    public function allBatal(){
        return $this->db->query("SELECT * FROM pemesanan JOIN pelanggan ON pelanggan.id_pelanggan = pemesanan.id_pelanggan WHERE status in (1,6) AND tanggal not like '%".date('Y-m-d')."%' ORDER BY tanggal = 'desc' ")->result();

    }

    public function find($id) {
    
        return $this->db->join('pelanggan', 'pelanggan.id_pelanggan = pemesanan.id_pelanggan')->get_where($this->table, ['id_pemesanan' => $id])->row();
    }

    public function pesanan($id_pelanggan) {
        $query = $this->db->join('pelanggan', 'pelanggan.id_pelanggan = pemesanan.id_pelanggan')->get_where($this->table, ['pemesanan.id_pelanggan' => $id_pelanggan])->result();
        return $query;
    }

    public function update($id){
        $post = $this->input->post();
        $this->db->update($this->table, $post, array('id_pemesanan' => $id));
    }

    public function storePemesanan(){
        $post = $this->input->post();  
        $file_upload = $this->upload_files();

        $post['tanggal'] = date('Y-m-d H:i:s');
        $post['file_pesanan'] = $file_upload[0];
        if (strpos($file_upload[1], '.') !== false) {
            $post['file_pesanan_2'] = $file_upload[1];
        } else {
            $post['file_pesanan_2'] = null;
        }

        $post['format'] = $post['panjang'] . ' x ' .$post['lebar'] . ' cm';

        $post['id_pelanggan'] = $this->session->userdata('id_pelanggan');
        $post['status'] = 1;

        unset($post['format_disabled']);

        unset($post['panjang']);

        unset($post['lebar']);

         

        $this->db->insert($this->table, $post);
    }

    public function upload_files()
    {       
        $file_name = [];
        $config = array();
        $config['upload_path']   = './assets/upload/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name']     = uniqid();
        $config['max_size']      = '0';
        $config['overwrite']     = FALSE;

        $countfiles = count($_FILES['file_pesanan']['name']);

        $this->load->library('upload');

        $files = $_FILES;
        for($i=0; $i < $countfiles; $i++)
        {           
            $_FILES['file_pesanan']['name']= $files['file_pesanan']['name'][$i];
            $_FILES['file_pesanan']['type']= $files['file_pesanan']['type'][$i];
            $_FILES['file_pesanan']['tmp_name']= $files['file_pesanan']['tmp_name'][$i];
            $_FILES['file_pesanan']['error']= $files['file_pesanan']['error'][$i];
            $_FILES['file_pesanan']['size']= $files['file_pesanan']['size'][$i];    

            $this->upload->initialize($config);
            $this->upload->do_upload('file_pesanan');

            $file_name[] = $this->upload->data('file_name');
        }

        return $file_name;
    }


    // laporan
    public function laporanAll() {
        $tgl_a = '';
        $tgl_b = '';

        if(!empty($_GET)){
            $tgl_a = date('Y-m-d H-i-s', strtotime($_GET['tgl_a']));
            $tgl_b = date('Y-m-d H-i-s', strtotime('+1 day', strtotime($_GET['tgl_b'])));
        }
        
        return $this->db->join('pelanggan', 'pelanggan.id_pelanggan = pemesanan.id_pelanggan')->where('status >', '2')->where('tanggal >', $tgl_a)->where('tanggal <', $tgl_b)->order_by('tanggal', 'desc')->get($this->table)->result();
    }

    // rules
    public function rulesKartuNama(){
        return [
            [
                'field' => 'sisi_cetak',
                'label' => 'Sisi Cetak',
                'rules' => 'required'
            ],
            [
                'field' => 'quantity',
                'label' => 'Quantity',
                'rules' => 'required|numeric'
            ],
            [
                'field' => 'jenis_kertas',
                'label' => 'Jenis Kertas',
                'rules' => 'required'
            ],
            [
                'field' => 'finishing',
                'label' => 'Finishing',
                'rules' => 'required'
            ],
            [
                'field' => 'total_biaya',
                'label' => 'Total Biaya',
                'rules' => 'required'
            ]
        ];
    }

    public function rulesBrosur(){
        return [
            
            [
                'field' => 'sisi_cetak',
                'label' => 'Sisi Cetak',
                'rules' => 'required'
            ],
            [
                'field' => 'quantity',
                'label' => 'Quantity',
                'rules' => 'required|numeric'
            ],
            [
                'field' => 'jenis_kertas',
                'label' => 'Jenis Kertas',
                'rules' => 'required'
            ],
            [
                'field' => 'total_biaya',
                'label' => 'Total Biaya',
                'rules' => 'required'
            ]   
        ];
    }

    public function rulesStiker(){
        return [
            
            [
                'field' => 'sisi_cetak',
                'label' => 'Sisi Cetak',
                'rules' => 'required'
            ],
            [
                'field' => 'quantity',
                'label' => 'Quantity',
                'rules' => 'required|numeric'
            ],
            [
                'field' => 'jenis_kertas',
                'label' => 'Jenis Kertas',
                'rules' => 'required'
            ],
            [
                'field' => 'total_biaya',
                'label' => 'Total Biaya',
                'rules' => 'required'
            ]   
        ];
    }

    public function delete($id)
    {
        return $this->db->delete($this->table, array("id_pemesanan" => $id));
    }

    
}