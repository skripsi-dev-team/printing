<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_model extends CI_Model {

//dilihat 0 = belum dilihat (false)
	//dilihat 1 = sudah dilihat (true)
	/*
		untuk 1 = kasir
		untuk 2 = petugas
	 */
	public function unReadNotif($level){
		$jml = $this->db->where('dilihat', 0)->where('untuk', $level)->from('notifikasi')->count_all_results();
		if($jml > 0){
			return $jml;
		} else {
			return '';
		}
	}

	public function getNotification($level){
		$q = $this->db->where('untuk', $level)->order_by('tanggal', 'desc')->get('notifikasi')->result();

		return $q;
	}
}